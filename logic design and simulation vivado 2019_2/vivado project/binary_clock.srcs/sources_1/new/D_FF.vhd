----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.09.2020 16:48:23
-- Design Name: 
-- Module Name: D_FF - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity D_FF is
    Port ( D : in STD_LOGIC;
           CLK : in STD_LOGIC;
           R : in STD_LOGIC;
           Q : out STD_LOGIC;
           Qbar : out STD_LOGIC);
end D_FF;

architecture Behavioral of D_FF is

signal Qtemp : STD_LOGIC := '0';

begin

    Q <= Qtemp;
    Qbar <= not Qtemp;

    process(CLK,D,R) begin
        if R = '1' then
            Qtemp <= '0';
        elsif rising_edge(CLK) then
            if D = '1' then
                Qtemp <= '1';
            else Qtemp <= '0';
            end if;
        end if;
    end process;

end Behavioral;
