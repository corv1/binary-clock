
################################################################
# This is a generated script based on design: design_1
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2019.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_1_script.tcl


# The design that will be created by this Tcl script contains the following 
# module references:
# D_FF, D_FF, D_FF, D_FF, D_FF, D_FF, D_FF, D_FF, D_FF, D_FF, D_FF, D_FF, D_FF, D_FF, D_FF, D_FF, D_FF, D_FF, D_FF

# Please add the sources of those modules before sourcing this Tcl script.

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7s100fgga676-2
   set_property BOARD_PART xilinx.com:sp701:part0:1.0 [current_project]
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name design_1

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

  # Add USER_COMMENTS on $design_name
  set_property USER_COMMENTS.comment_0 "Q initially low" [get_bd_designs $design_name]
  set_property USER_COMMENTS.comment_1 "Q high for hours
Q low for minutes" [get_bd_designs $design_name]

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: seconds_counter
proc create_hier_cell_seconds_counter { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_seconds_counter() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I CLK
  create_bd_pin -dir O -from 0 -to 0 minute_passed
  create_bd_pin -dir O -from 5 -to 0 second

  # Create instance: D_FF_0, and set properties
  set block_name D_FF
  set block_cell_name D_FF_0
  if { [catch {set D_FF_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: D_FF_1, and set properties
  set block_name D_FF
  set block_cell_name D_FF_1
  if { [catch {set D_FF_1 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_1 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: D_FF_2, and set properties
  set block_name D_FF
  set block_cell_name D_FF_2
  if { [catch {set D_FF_2 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_2 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: D_FF_3, and set properties
  set block_name D_FF
  set block_cell_name D_FF_3
  if { [catch {set D_FF_3 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_3 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: D_FF_4, and set properties
  set block_name D_FF
  set block_cell_name D_FF_4
  if { [catch {set D_FF_4 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_4 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: D_FF_5, and set properties
  set block_name D_FF
  set block_cell_name D_FF_5
  if { [catch {set D_FF_5 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_5 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: util_vector_logic_0, and set properties
  set util_vector_logic_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_0 ]
  set_property -dict [ list \
   CONFIG.C_SIZE {1} \
 ] $util_vector_logic_0

  # Create instance: util_vector_logic_1, and set properties
  set util_vector_logic_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_1 ]
  set_property -dict [ list \
   CONFIG.C_SIZE {1} \
 ] $util_vector_logic_1

  # Create instance: util_vector_logic_2, and set properties
  set util_vector_logic_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_2 ]
  set_property -dict [ list \
   CONFIG.C_SIZE {1} \
 ] $util_vector_logic_2

  # Create instance: xlconcat_0, and set properties
  set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0 ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {6} \
 ] $xlconcat_0

  # Create port connections
  connect_bd_net -net CLK_1 [get_bd_pins CLK] [get_bd_pins D_FF_0/CLK]
  connect_bd_net -net D_FF_0_Q [get_bd_pins D_FF_0/Q] [get_bd_pins xlconcat_0/In0]
  connect_bd_net -net D_FF_0_Qbar [get_bd_pins D_FF_0/D] [get_bd_pins D_FF_0/Qbar] [get_bd_pins D_FF_1/CLK]
  connect_bd_net -net D_FF_1_Q [get_bd_pins D_FF_1/Q] [get_bd_pins xlconcat_0/In1]
  connect_bd_net -net D_FF_1_Qbar [get_bd_pins D_FF_1/D] [get_bd_pins D_FF_1/Qbar] [get_bd_pins D_FF_2/CLK]
  connect_bd_net -net D_FF_2_Q [get_bd_pins D_FF_2/Q] [get_bd_pins util_vector_logic_0/Op1] [get_bd_pins xlconcat_0/In2]
  connect_bd_net -net D_FF_2_Qbar [get_bd_pins D_FF_2/D] [get_bd_pins D_FF_2/Qbar] [get_bd_pins D_FF_3/CLK]
  connect_bd_net -net D_FF_3_Q [get_bd_pins D_FF_3/Q] [get_bd_pins util_vector_logic_0/Op2] [get_bd_pins xlconcat_0/In3]
  connect_bd_net -net D_FF_3_Qbar [get_bd_pins D_FF_3/D] [get_bd_pins D_FF_3/Qbar] [get_bd_pins D_FF_4/CLK]
  connect_bd_net -net D_FF_4_Q [get_bd_pins D_FF_4/Q] [get_bd_pins util_vector_logic_1/Op1] [get_bd_pins xlconcat_0/In4]
  connect_bd_net -net D_FF_4_Qbar [get_bd_pins D_FF_4/D] [get_bd_pins D_FF_4/Qbar] [get_bd_pins D_FF_5/CLK]
  connect_bd_net -net D_FF_5_Q [get_bd_pins D_FF_5/Q] [get_bd_pins util_vector_logic_1/Op2] [get_bd_pins xlconcat_0/In5]
  connect_bd_net -net D_FF_5_Qbar [get_bd_pins D_FF_5/D] [get_bd_pins D_FF_5/Qbar]
  connect_bd_net -net util_vector_logic_0_Res [get_bd_pins util_vector_logic_0/Res] [get_bd_pins util_vector_logic_2/Op1]
  connect_bd_net -net util_vector_logic_1_Res [get_bd_pins util_vector_logic_1/Res] [get_bd_pins util_vector_logic_2/Op2]
  connect_bd_net -net util_vector_logic_2_Res [get_bd_pins minute_passed] [get_bd_pins D_FF_0/R] [get_bd_pins D_FF_1/R] [get_bd_pins D_FF_2/R] [get_bd_pins D_FF_3/R] [get_bd_pins D_FF_4/R] [get_bd_pins D_FF_5/R] [get_bd_pins util_vector_logic_2/Res]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins second] [get_bd_pins xlconcat_0/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: minutes_counter
proc create_hier_cell_minutes_counter { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_minutes_counter() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I CLK
  create_bd_pin -dir O -from 0 -to 0 hour_passed
  create_bd_pin -dir O -from 5 -to 0 minutes

  # Create instance: D_FF_0, and set properties
  set block_name D_FF
  set block_cell_name D_FF_0
  if { [catch {set D_FF_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: D_FF_1, and set properties
  set block_name D_FF
  set block_cell_name D_FF_1
  if { [catch {set D_FF_1 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_1 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: D_FF_2, and set properties
  set block_name D_FF
  set block_cell_name D_FF_2
  if { [catch {set D_FF_2 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_2 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: D_FF_3, and set properties
  set block_name D_FF
  set block_cell_name D_FF_3
  if { [catch {set D_FF_3 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_3 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: D_FF_4, and set properties
  set block_name D_FF
  set block_cell_name D_FF_4
  if { [catch {set D_FF_4 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_4 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: D_FF_5, and set properties
  set block_name D_FF
  set block_cell_name D_FF_5
  if { [catch {set D_FF_5 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_5 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: util_vector_logic_0, and set properties
  set util_vector_logic_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_0 ]
  set_property -dict [ list \
   CONFIG.C_SIZE {1} \
 ] $util_vector_logic_0

  # Create instance: util_vector_logic_1, and set properties
  set util_vector_logic_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_1 ]
  set_property -dict [ list \
   CONFIG.C_SIZE {1} \
 ] $util_vector_logic_1

  # Create instance: util_vector_logic_2, and set properties
  set util_vector_logic_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_2 ]
  set_property -dict [ list \
   CONFIG.C_SIZE {1} \
 ] $util_vector_logic_2

  # Create instance: xlconcat_0, and set properties
  set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0 ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {6} \
 ] $xlconcat_0

  # Create port connections
  connect_bd_net -net CLK_1 [get_bd_pins CLK] [get_bd_pins D_FF_0/CLK]
  connect_bd_net -net D_FF_0_Q [get_bd_pins D_FF_0/Q] [get_bd_pins xlconcat_0/In0]
  connect_bd_net -net D_FF_0_Qbar [get_bd_pins D_FF_0/D] [get_bd_pins D_FF_0/Qbar] [get_bd_pins D_FF_1/CLK]
  connect_bd_net -net D_FF_1_Q [get_bd_pins D_FF_1/Q] [get_bd_pins xlconcat_0/In1]
  connect_bd_net -net D_FF_1_Qbar [get_bd_pins D_FF_1/D] [get_bd_pins D_FF_1/Qbar] [get_bd_pins D_FF_2/CLK]
  connect_bd_net -net D_FF_2_Q [get_bd_pins D_FF_2/Q] [get_bd_pins util_vector_logic_0/Op1] [get_bd_pins xlconcat_0/In2]
  connect_bd_net -net D_FF_2_Qbar [get_bd_pins D_FF_2/D] [get_bd_pins D_FF_2/Qbar] [get_bd_pins D_FF_3/CLK]
  connect_bd_net -net D_FF_3_Q [get_bd_pins D_FF_3/Q] [get_bd_pins util_vector_logic_0/Op2] [get_bd_pins xlconcat_0/In3]
  connect_bd_net -net D_FF_3_Qbar [get_bd_pins D_FF_3/D] [get_bd_pins D_FF_3/Qbar] [get_bd_pins D_FF_4/CLK]
  connect_bd_net -net D_FF_4_Q [get_bd_pins D_FF_4/Q] [get_bd_pins util_vector_logic_1/Op1] [get_bd_pins xlconcat_0/In4]
  connect_bd_net -net D_FF_4_Qbar [get_bd_pins D_FF_4/D] [get_bd_pins D_FF_4/Qbar] [get_bd_pins D_FF_5/CLK]
  connect_bd_net -net D_FF_5_Q [get_bd_pins D_FF_5/Q] [get_bd_pins util_vector_logic_1/Op2] [get_bd_pins xlconcat_0/In5]
  connect_bd_net -net D_FF_5_Qbar [get_bd_pins D_FF_5/D] [get_bd_pins D_FF_5/Qbar]
  connect_bd_net -net util_vector_logic_0_Res [get_bd_pins util_vector_logic_0/Res] [get_bd_pins util_vector_logic_2/Op1]
  connect_bd_net -net util_vector_logic_1_Res [get_bd_pins util_vector_logic_1/Res] [get_bd_pins util_vector_logic_2/Op2]
  connect_bd_net -net util_vector_logic_2_Res [get_bd_pins hour_passed] [get_bd_pins D_FF_0/R] [get_bd_pins D_FF_1/R] [get_bd_pins D_FF_2/R] [get_bd_pins D_FF_3/R] [get_bd_pins D_FF_4/R] [get_bd_pins D_FF_5/R] [get_bd_pins util_vector_logic_2/Res]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins minutes] [get_bd_pins xlconcat_0/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: hours_counter
proc create_hier_cell_hours_counter { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_hours_counter() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I CLK
  create_bd_pin -dir O -from 4 -to 0 hours

  # Create instance: D_FF_0, and set properties
  set block_name D_FF
  set block_cell_name D_FF_0
  if { [catch {set D_FF_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: D_FF_1, and set properties
  set block_name D_FF
  set block_cell_name D_FF_1
  if { [catch {set D_FF_1 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_1 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: D_FF_2, and set properties
  set block_name D_FF
  set block_cell_name D_FF_2
  if { [catch {set D_FF_2 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_2 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: D_FF_3, and set properties
  set block_name D_FF
  set block_cell_name D_FF_3
  if { [catch {set D_FF_3 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_3 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: D_FF_4, and set properties
  set block_name D_FF
  set block_cell_name D_FF_4
  if { [catch {set D_FF_4 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_4 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: util_vector_logic_0, and set properties
  set util_vector_logic_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_0 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {and} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_andgate.png} \
 ] $util_vector_logic_0

  # Create instance: xlconcat_0, and set properties
  set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0 ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {5} \
 ] $xlconcat_0

  # Create port connections
  connect_bd_net -net CLK_1 [get_bd_pins CLK] [get_bd_pins D_FF_0/CLK]
  connect_bd_net -net D_FF_0_Q [get_bd_pins D_FF_0/Q] [get_bd_pins xlconcat_0/In0]
  connect_bd_net -net D_FF_0_Qbar [get_bd_pins D_FF_0/D] [get_bd_pins D_FF_0/Qbar] [get_bd_pins D_FF_1/CLK]
  connect_bd_net -net D_FF_1_Q [get_bd_pins D_FF_1/Q] [get_bd_pins xlconcat_0/In1]
  connect_bd_net -net D_FF_1_Qbar [get_bd_pins D_FF_1/D] [get_bd_pins D_FF_1/Qbar] [get_bd_pins D_FF_2/CLK]
  connect_bd_net -net D_FF_2_Q [get_bd_pins D_FF_2/Q] [get_bd_pins xlconcat_0/In2]
  connect_bd_net -net D_FF_2_Qbar [get_bd_pins D_FF_2/D] [get_bd_pins D_FF_2/Qbar] [get_bd_pins D_FF_3/CLK]
  connect_bd_net -net D_FF_3_Q [get_bd_pins D_FF_3/Q] [get_bd_pins util_vector_logic_0/Op2] [get_bd_pins xlconcat_0/In3]
  connect_bd_net -net D_FF_3_Qbar [get_bd_pins D_FF_3/D] [get_bd_pins D_FF_3/Qbar] [get_bd_pins D_FF_4/CLK]
  connect_bd_net -net D_FF_4_Q [get_bd_pins D_FF_4/Q] [get_bd_pins util_vector_logic_0/Op1] [get_bd_pins xlconcat_0/In4]
  connect_bd_net -net D_FF_4_Qbar [get_bd_pins D_FF_4/D] [get_bd_pins D_FF_4/Qbar]
  connect_bd_net -net util_vector_logic_0_Res [get_bd_pins D_FF_0/R] [get_bd_pins D_FF_1/R] [get_bd_pins D_FF_2/R] [get_bd_pins D_FF_3/R] [get_bd_pins D_FF_4/R] [get_bd_pins util_vector_logic_0/Res]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins hours] [get_bd_pins xlconcat_0/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports

  # Create ports
  set CLK [ create_bd_port -dir I CLK ]
  set enable_button [ create_bd_port -dir I enable_button ]
  set hours [ create_bd_port -dir O -from 4 -to 0 hours ]
  set minutes [ create_bd_port -dir O -from 5 -to 0 minutes ]
  set seconds [ create_bd_port -dir O -from 5 -to 0 seconds ]
  set select_button [ create_bd_port -dir I select_button ]
  set set_button [ create_bd_port -dir I set_button ]

  # Create instance: D_FF_0, and set properties
  set block_name D_FF
  set block_cell_name D_FF_0
  if { [catch {set D_FF_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: D_FF_1, and set properties
  set block_name D_FF
  set block_cell_name D_FF_1
  if { [catch {set D_FF_1 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $D_FF_1 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: hours_counter
  create_hier_cell_hours_counter [current_bd_instance .] hours_counter

  # Create instance: minutes_counter
  create_hier_cell_minutes_counter [current_bd_instance .] minutes_counter

  # Create instance: seconds_counter
  create_hier_cell_seconds_counter [current_bd_instance .] seconds_counter

  # Create instance: util_vector_logic_0, and set properties
  set util_vector_logic_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_0 ]
  set_property -dict [ list \
   CONFIG.C_SIZE {1} \
 ] $util_vector_logic_0

  # Create instance: util_vector_logic_1, and set properties
  set util_vector_logic_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_1 ]
  set_property -dict [ list \
   CONFIG.C_SIZE {1} \
 ] $util_vector_logic_1

  # Create instance: util_vector_logic_2, and set properties
  set util_vector_logic_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_2 ]
  set_property -dict [ list \
   CONFIG.C_SIZE {1} \
 ] $util_vector_logic_2

  # Create instance: util_vector_logic_3, and set properties
  set util_vector_logic_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_3 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {or} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_orgate.png} \
 ] $util_vector_logic_3

  # Create instance: util_vector_logic_4, and set properties
  set util_vector_logic_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_4 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {or} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_orgate.png} \
 ] $util_vector_logic_4

  # Create instance: util_vector_logic_5, and set properties
  set util_vector_logic_5 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_5 ]
  set_property -dict [ list \
   CONFIG.C_SIZE {1} \
 ] $util_vector_logic_5

  # Create instance: util_vector_logic_6, and set properties
  set util_vector_logic_6 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_6 ]
  set_property -dict [ list \
   CONFIG.C_SIZE {1} \
 ] $util_vector_logic_6

  # Create instance: util_vector_logic_7, and set properties
  set util_vector_logic_7 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_7 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {xor} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_xorgate.png} \
 ] $util_vector_logic_7

  # Create instance: util_vector_logic_8, and set properties
  set util_vector_logic_8 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_8 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {xor} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_xorgate.png} \
 ] $util_vector_logic_8

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $xlconstant_0

  # Create instance: xlconstant_1, and set properties
  set xlconstant_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_1 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
 ] $xlconstant_1

  # Create port connections
  connect_bd_net -net CLK_1 [get_bd_ports CLK] [get_bd_pins util_vector_logic_0/Op1]
  connect_bd_net -net D_FF_0_Q [get_bd_pins D_FF_0/Q] [get_bd_pins util_vector_logic_1/Op1] [get_bd_pins util_vector_logic_2/Op1] [get_bd_pins util_vector_logic_7/Op1]
  connect_bd_net -net D_FF_0_Qbar [get_bd_pins D_FF_0/Qbar] [get_bd_pins util_vector_logic_0/Op2]
  connect_bd_net -net D_FF_1_Q [get_bd_pins D_FF_1/Q] [get_bd_pins util_vector_logic_1/Op2] [get_bd_pins util_vector_logic_8/Op2]
  connect_bd_net -net D_FF_1_Qbar [get_bd_pins D_FF_1/Qbar] [get_bd_pins util_vector_logic_2/Op2]
  connect_bd_net -net hours_counter_hours [get_bd_ports hours] [get_bd_pins hours_counter/hours]
  connect_bd_net -net minutes_counter_hour_passed [get_bd_pins minutes_counter/hour_passed] [get_bd_pins util_vector_logic_3/Op2]
  connect_bd_net -net seconds_counter_minute_passed [get_bd_pins seconds_counter/minute_passed] [get_bd_pins util_vector_logic_4/Op2]
  connect_bd_net -net seconds_counter_seconds [get_bd_ports seconds] [get_bd_pins seconds_counter/second]
  connect_bd_net -net seconds_counter_seconds1 [get_bd_ports minutes] [get_bd_pins minutes_counter/minutes]
  connect_bd_net -net select_bu_1 [get_bd_ports select_button] [get_bd_pins D_FF_1/CLK]
  connect_bd_net -net set_button_1 [get_bd_ports set_button] [get_bd_pins util_vector_logic_5/Op1] [get_bd_pins util_vector_logic_6/Op2]
  connect_bd_net -net set_or_count_button [get_bd_ports enable_button] [get_bd_pins D_FF_0/CLK]
  connect_bd_net -net util_vector_logic_0_Res [get_bd_pins seconds_counter/CLK] [get_bd_pins util_vector_logic_0/Res]
  connect_bd_net -net util_vector_logic_1_Res [get_bd_pins util_vector_logic_1/Res] [get_bd_pins util_vector_logic_6/Op1]
  connect_bd_net -net util_vector_logic_2_Res [get_bd_pins util_vector_logic_2/Res] [get_bd_pins util_vector_logic_5/Op2]
  connect_bd_net -net util_vector_logic_3_Res [get_bd_pins hours_counter/CLK] [get_bd_pins util_vector_logic_3/Res]
  connect_bd_net -net util_vector_logic_4_Res [get_bd_pins minutes_counter/CLK] [get_bd_pins util_vector_logic_4/Res]
  connect_bd_net -net util_vector_logic_5_Res [get_bd_pins util_vector_logic_4/Op1] [get_bd_pins util_vector_logic_5/Res]
  connect_bd_net -net util_vector_logic_6_Res [get_bd_pins util_vector_logic_3/Op1] [get_bd_pins util_vector_logic_6/Res]
  connect_bd_net -net util_vector_logic_7_Res [get_bd_pins D_FF_0/D] [get_bd_pins util_vector_logic_7/Res]
  connect_bd_net -net util_vector_logic_8_Res [get_bd_pins D_FF_1/D] [get_bd_pins util_vector_logic_8/Res]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins D_FF_0/R] [get_bd_pins D_FF_1/R] [get_bd_pins xlconstant_0/dout]
  connect_bd_net -net xlconstant_1_dout [get_bd_pins util_vector_logic_7/Op2] [get_bd_pins util_vector_logic_8/Op1] [get_bd_pins xlconstant_1/dout]

  # Create address segments


  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


