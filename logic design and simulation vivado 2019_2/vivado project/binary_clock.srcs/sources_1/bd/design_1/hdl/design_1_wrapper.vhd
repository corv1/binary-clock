--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
--Date        : Mon Sep 28 22:44:57 2020
--Host        : DESKTOP-SOQ5SQJ running 64-bit major release  (build 9200)
--Command     : generate_target design_1_wrapper.bd
--Design      : design_1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_wrapper is
  port (
    CLK : in STD_LOGIC;
    enable_button : in STD_LOGIC;
    hours : out STD_LOGIC_VECTOR ( 4 downto 0 );
    minutes : out STD_LOGIC_VECTOR ( 5 downto 0 );
    seconds : out STD_LOGIC_VECTOR ( 5 downto 0 );
    select_button : in STD_LOGIC;
    set_button : in STD_LOGIC
  );
end design_1_wrapper;

architecture STRUCTURE of design_1_wrapper is
  component design_1 is
  port (
    CLK : in STD_LOGIC;
    seconds : out STD_LOGIC_VECTOR ( 5 downto 0 );
    minutes : out STD_LOGIC_VECTOR ( 5 downto 0 );
    hours : out STD_LOGIC_VECTOR ( 4 downto 0 );
    enable_button : in STD_LOGIC;
    select_button : in STD_LOGIC;
    set_button : in STD_LOGIC
  );
  end component design_1;
begin
design_1_i: component design_1
     port map (
      CLK => CLK,
      enable_button => enable_button,
      hours(4 downto 0) => hours(4 downto 0),
      minutes(5 downto 0) => minutes(5 downto 0),
      seconds(5 downto 0) => seconds(5 downto 0),
      select_button => select_button,
      set_button => set_button
    );
end STRUCTURE;
