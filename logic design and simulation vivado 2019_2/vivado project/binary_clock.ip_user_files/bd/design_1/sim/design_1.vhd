--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
--Date        : Mon Sep 28 21:33:54 2020
--Host        : DESKTOP-SOQ5SQJ running 64-bit major release  (build 9200)
--Command     : generate_target design_1.bd
--Design      : design_1
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity hours_counter_imp_JGOEVX is
  port (
    CLK : in STD_LOGIC;
    hours : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
end hours_counter_imp_JGOEVX;

architecture STRUCTURE of hours_counter_imp_JGOEVX is
  component design_1_D_FF_0_9 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_0_9;
  component design_1_D_FF_0_10 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_0_10;
  component design_1_D_FF_0_11 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_0_11;
  component design_1_D_FF_2_2 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_2_2;
  component design_1_D_FF_2_3 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_2_3;
  component design_1_util_vector_logic_0_4 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Op2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_vector_logic_0_4;
  component design_1_xlconcat_0_3 is
  port (
    In0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In3 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In4 : in STD_LOGIC_VECTOR ( 0 to 0 );
    dout : out STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  end component design_1_xlconcat_0_3;
  signal CLK_1 : STD_LOGIC;
  signal D_FF_0_Q : STD_LOGIC;
  signal D_FF_0_Qbar : STD_LOGIC;
  signal D_FF_1_Q : STD_LOGIC;
  signal D_FF_1_Qbar : STD_LOGIC;
  signal D_FF_2_Q : STD_LOGIC;
  signal D_FF_2_Qbar : STD_LOGIC;
  signal D_FF_3_Q : STD_LOGIC;
  signal D_FF_3_Qbar : STD_LOGIC;
  signal D_FF_4_Q : STD_LOGIC;
  signal D_FF_4_Qbar : STD_LOGIC;
  signal util_vector_logic_0_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xlconcat_0_dout : STD_LOGIC_VECTOR ( 4 downto 0 );
begin
  CLK_1 <= CLK;
  hours(4 downto 0) <= xlconcat_0_dout(4 downto 0);
D_FF_0: component design_1_D_FF_0_9
     port map (
      CLK => CLK_1,
      D => D_FF_0_Qbar,
      Q => D_FF_0_Q,
      Qbar => D_FF_0_Qbar,
      R => util_vector_logic_0_Res(0)
    );
D_FF_1: component design_1_D_FF_0_10
     port map (
      CLK => D_FF_0_Qbar,
      D => D_FF_1_Qbar,
      Q => D_FF_1_Q,
      Qbar => D_FF_1_Qbar,
      R => util_vector_logic_0_Res(0)
    );
D_FF_2: component design_1_D_FF_0_11
     port map (
      CLK => D_FF_1_Qbar,
      D => D_FF_2_Qbar,
      Q => D_FF_2_Q,
      Qbar => D_FF_2_Qbar,
      R => util_vector_logic_0_Res(0)
    );
D_FF_3: component design_1_D_FF_2_2
     port map (
      CLK => D_FF_2_Qbar,
      D => D_FF_3_Qbar,
      Q => D_FF_3_Q,
      Qbar => D_FF_3_Qbar,
      R => util_vector_logic_0_Res(0)
    );
D_FF_4: component design_1_D_FF_2_3
     port map (
      CLK => D_FF_3_Qbar,
      D => D_FF_4_Qbar,
      Q => D_FF_4_Q,
      Qbar => D_FF_4_Qbar,
      R => util_vector_logic_0_Res(0)
    );
util_vector_logic_0: component design_1_util_vector_logic_0_4
     port map (
      Op1(0) => D_FF_4_Q,
      Op2(0) => D_FF_3_Q,
      Res(0) => util_vector_logic_0_Res(0)
    );
xlconcat_0: component design_1_xlconcat_0_3
     port map (
      In0(0) => D_FF_0_Q,
      In1(0) => D_FF_1_Q,
      In2(0) => D_FF_2_Q,
      In3(0) => D_FF_3_Q,
      In4(0) => D_FF_4_Q,
      dout(4 downto 0) => xlconcat_0_dout(4 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity minutes_counter_imp_CMV4NT is
  port (
    CLK : in STD_LOGIC;
    hour_passed : out STD_LOGIC_VECTOR ( 0 to 0 );
    minutes : out STD_LOGIC_VECTOR ( 5 downto 0 )
  );
end minutes_counter_imp_CMV4NT;

architecture STRUCTURE of minutes_counter_imp_CMV4NT is
  component design_1_xlconcat_0_1 is
  port (
    In0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In3 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In4 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In5 : in STD_LOGIC_VECTOR ( 0 to 0 );
    dout : out STD_LOGIC_VECTOR ( 5 downto 0 )
  );
  end component design_1_xlconcat_0_1;
  component design_1_util_vector_logic_0_3 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Op2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_vector_logic_0_3;
  component design_1_util_vector_logic_1_0 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Op2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_vector_logic_1_0;
  component design_1_util_vector_logic_2_0 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Op2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_vector_logic_2_0;
  component design_1_D_FF_0_8 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_0_8;
  component design_1_D_FF_1_0 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_1_0;
  component design_1_D_FF_2_1 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_2_1;
  component design_1_D_FF_3_0 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_3_0;
  component design_1_D_FF_4_0 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_4_0;
  component design_1_D_FF_5_1 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_5_1;
  signal CLK_1 : STD_LOGIC;
  signal D_FF_0_Q : STD_LOGIC;
  signal D_FF_0_Qbar : STD_LOGIC;
  signal D_FF_1_Q : STD_LOGIC;
  signal D_FF_1_Qbar : STD_LOGIC;
  signal D_FF_2_Q : STD_LOGIC;
  signal D_FF_2_Qbar : STD_LOGIC;
  signal D_FF_3_Q : STD_LOGIC;
  signal D_FF_3_Qbar : STD_LOGIC;
  signal D_FF_4_Q : STD_LOGIC;
  signal D_FF_4_Qbar : STD_LOGIC;
  signal D_FF_5_Q : STD_LOGIC;
  signal D_FF_5_Qbar : STD_LOGIC;
  signal util_vector_logic_0_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal util_vector_logic_1_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal util_vector_logic_2_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xlconcat_0_dout : STD_LOGIC_VECTOR ( 5 downto 0 );
begin
  CLK_1 <= CLK;
  hour_passed(0) <= util_vector_logic_2_Res(0);
  minutes(5 downto 0) <= xlconcat_0_dout(5 downto 0);
D_FF_0: component design_1_D_FF_0_8
     port map (
      CLK => CLK_1,
      D => D_FF_0_Qbar,
      Q => D_FF_0_Q,
      Qbar => D_FF_0_Qbar,
      R => util_vector_logic_2_Res(0)
    );
D_FF_1: component design_1_D_FF_1_0
     port map (
      CLK => D_FF_0_Qbar,
      D => D_FF_1_Qbar,
      Q => D_FF_1_Q,
      Qbar => D_FF_1_Qbar,
      R => util_vector_logic_2_Res(0)
    );
D_FF_2: component design_1_D_FF_2_1
     port map (
      CLK => D_FF_1_Qbar,
      D => D_FF_2_Qbar,
      Q => D_FF_2_Q,
      Qbar => D_FF_2_Qbar,
      R => util_vector_logic_2_Res(0)
    );
D_FF_3: component design_1_D_FF_3_0
     port map (
      CLK => D_FF_2_Qbar,
      D => D_FF_3_Qbar,
      Q => D_FF_3_Q,
      Qbar => D_FF_3_Qbar,
      R => util_vector_logic_2_Res(0)
    );
D_FF_4: component design_1_D_FF_4_0
     port map (
      CLK => D_FF_3_Qbar,
      D => D_FF_4_Qbar,
      Q => D_FF_4_Q,
      Qbar => D_FF_4_Qbar,
      R => util_vector_logic_2_Res(0)
    );
D_FF_5: component design_1_D_FF_5_1
     port map (
      CLK => D_FF_4_Qbar,
      D => D_FF_5_Qbar,
      Q => D_FF_5_Q,
      Qbar => D_FF_5_Qbar,
      R => util_vector_logic_2_Res(0)
    );
util_vector_logic_0: component design_1_util_vector_logic_0_3
     port map (
      Op1(0) => D_FF_2_Q,
      Op2(0) => D_FF_3_Q,
      Res(0) => util_vector_logic_0_Res(0)
    );
util_vector_logic_1: component design_1_util_vector_logic_1_0
     port map (
      Op1(0) => D_FF_4_Q,
      Op2(0) => D_FF_5_Q,
      Res(0) => util_vector_logic_1_Res(0)
    );
util_vector_logic_2: component design_1_util_vector_logic_2_0
     port map (
      Op1(0) => util_vector_logic_0_Res(0),
      Op2(0) => util_vector_logic_1_Res(0),
      Res(0) => util_vector_logic_2_Res(0)
    );
xlconcat_0: component design_1_xlconcat_0_1
     port map (
      In0(0) => D_FF_0_Q,
      In1(0) => D_FF_1_Q,
      In2(0) => D_FF_2_Q,
      In3(0) => D_FF_3_Q,
      In4(0) => D_FF_4_Q,
      In5(0) => D_FF_5_Q,
      dout(5 downto 0) => xlconcat_0_dout(5 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity seconds_counter_imp_1FQ6MQM is
  port (
    CLK : in STD_LOGIC;
    minute_passed : out STD_LOGIC_VECTOR ( 0 to 0 );
    second : out STD_LOGIC_VECTOR ( 5 downto 0 )
  );
end seconds_counter_imp_1FQ6MQM;

architecture STRUCTURE of seconds_counter_imp_1FQ6MQM is
  component design_1_xlconcat_0_0 is
  port (
    In0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In3 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In4 : in STD_LOGIC_VECTOR ( 0 to 0 );
    In5 : in STD_LOGIC_VECTOR ( 0 to 0 );
    dout : out STD_LOGIC_VECTOR ( 5 downto 0 )
  );
  end component design_1_xlconcat_0_0;
  component design_1_util_vector_logic_0_0 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Op2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_vector_logic_0_0;
  component design_1_util_vector_logic_0_1 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Op2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_vector_logic_0_1;
  component design_1_util_vector_logic_0_2 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Op2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_vector_logic_0_2;
  component design_1_D_FF_0_0 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_0_0;
  component design_1_D_FF_0_1 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_0_1;
  component design_1_D_FF_0_2 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_0_2;
  component design_1_D_FF_0_3 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_0_3;
  component design_1_D_FF_0_4 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_0_4;
  component design_1_D_FF_0_5 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_0_5;
  signal CLK_1 : STD_LOGIC;
  signal D_FF_0_Q : STD_LOGIC;
  signal D_FF_0_Qbar : STD_LOGIC;
  signal D_FF_1_Q : STD_LOGIC;
  signal D_FF_1_Qbar : STD_LOGIC;
  signal D_FF_2_Q : STD_LOGIC;
  signal D_FF_2_Qbar : STD_LOGIC;
  signal D_FF_3_Q : STD_LOGIC;
  signal D_FF_3_Qbar : STD_LOGIC;
  signal D_FF_4_Q : STD_LOGIC;
  signal D_FF_4_Qbar : STD_LOGIC;
  signal D_FF_5_Q : STD_LOGIC;
  signal D_FF_5_Qbar : STD_LOGIC;
  signal util_vector_logic_0_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal util_vector_logic_1_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal util_vector_logic_2_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xlconcat_0_dout : STD_LOGIC_VECTOR ( 5 downto 0 );
begin
  CLK_1 <= CLK;
  minute_passed(0) <= util_vector_logic_2_Res(0);
  second(5 downto 0) <= xlconcat_0_dout(5 downto 0);
D_FF_0: component design_1_D_FF_0_0
     port map (
      CLK => CLK_1,
      D => D_FF_0_Qbar,
      Q => D_FF_0_Q,
      Qbar => D_FF_0_Qbar,
      R => util_vector_logic_2_Res(0)
    );
D_FF_1: component design_1_D_FF_0_1
     port map (
      CLK => D_FF_0_Qbar,
      D => D_FF_1_Qbar,
      Q => D_FF_1_Q,
      Qbar => D_FF_1_Qbar,
      R => util_vector_logic_2_Res(0)
    );
D_FF_2: component design_1_D_FF_0_2
     port map (
      CLK => D_FF_1_Qbar,
      D => D_FF_2_Qbar,
      Q => D_FF_2_Q,
      Qbar => D_FF_2_Qbar,
      R => util_vector_logic_2_Res(0)
    );
D_FF_3: component design_1_D_FF_0_3
     port map (
      CLK => D_FF_2_Qbar,
      D => D_FF_3_Qbar,
      Q => D_FF_3_Q,
      Qbar => D_FF_3_Qbar,
      R => util_vector_logic_2_Res(0)
    );
D_FF_4: component design_1_D_FF_0_4
     port map (
      CLK => D_FF_3_Qbar,
      D => D_FF_4_Qbar,
      Q => D_FF_4_Q,
      Qbar => D_FF_4_Qbar,
      R => util_vector_logic_2_Res(0)
    );
D_FF_5: component design_1_D_FF_0_5
     port map (
      CLK => D_FF_4_Qbar,
      D => D_FF_5_Qbar,
      Q => D_FF_5_Q,
      Qbar => D_FF_5_Qbar,
      R => util_vector_logic_2_Res(0)
    );
util_vector_logic_0: component design_1_util_vector_logic_0_0
     port map (
      Op1(0) => D_FF_2_Q,
      Op2(0) => D_FF_3_Q,
      Res(0) => util_vector_logic_0_Res(0)
    );
util_vector_logic_1: component design_1_util_vector_logic_0_1
     port map (
      Op1(0) => D_FF_4_Q,
      Op2(0) => D_FF_5_Q,
      Res(0) => util_vector_logic_1_Res(0)
    );
util_vector_logic_2: component design_1_util_vector_logic_0_2
     port map (
      Op1(0) => util_vector_logic_0_Res(0),
      Op2(0) => util_vector_logic_1_Res(0),
      Res(0) => util_vector_logic_2_Res(0)
    );
xlconcat_0: component design_1_xlconcat_0_0
     port map (
      In0(0) => D_FF_0_Q,
      In1(0) => D_FF_1_Q,
      In2(0) => D_FF_2_Q,
      In3(0) => D_FF_3_Q,
      In4(0) => D_FF_4_Q,
      In5(0) => D_FF_5_Q,
      dout(5 downto 0) => xlconcat_0_dout(5 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
-- Q initially low
  -- Q high for hours
  -- Q low for minutes
  entity design_1 is
  port (
    CLK : in STD_LOGIC;
    enable_button : in STD_LOGIC;
    hours : out STD_LOGIC_VECTOR ( 4 downto 0 );
    minutes : out STD_LOGIC_VECTOR ( 5 downto 0 );
    seconds : out STD_LOGIC_VECTOR ( 5 downto 0 );
    select_button : in STD_LOGIC;
    set_button : in STD_LOGIC
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of design_1 : entity is "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=40,numReposBlks=37,numNonXlnxBlks=0,numHierBlks=3,maxHierDepth=1,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=19,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of design_1 : entity is "design_1.hwdef";
end design_1;

architecture STRUCTURE of design_1 is
  component design_1_D_FF_0_12 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_0_12;
  component design_1_util_vector_logic_0_5 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Op2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_vector_logic_0_5;
  component design_1_util_vector_logic_0_6 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Op2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_vector_logic_0_6;
  component design_1_D_FF_0_13 is
  port (
    D : in STD_LOGIC;
    CLK : in STD_LOGIC;
    R : in STD_LOGIC;
    Q : out STD_LOGIC;
    Qbar : out STD_LOGIC
  );
  end component design_1_D_FF_0_13;
  component design_1_util_vector_logic_1_1 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Op2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_vector_logic_1_1;
  component design_1_util_vector_logic_1_2 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Op2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_vector_logic_1_2;
  component design_1_util_vector_logic_3_0 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Op2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_vector_logic_3_0;
  component design_1_xlconstant_0_0 is
  port (
    dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_xlconstant_0_0;
  component design_1_util_vector_logic_2_1 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Op2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_vector_logic_2_1;
  component design_1_util_vector_logic_2_2 is
  port (
    Op1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Op2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    Res : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_vector_logic_2_2;
  signal CLK_1 : STD_LOGIC;
  signal D_FF_0_Q : STD_LOGIC;
  signal D_FF_0_Qbar : STD_LOGIC;
  signal D_FF_1_Q : STD_LOGIC;
  signal D_FF_1_Qbar : STD_LOGIC;
  signal hours_counter_hours : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal minutes_counter_hour_passed : STD_LOGIC_VECTOR ( 0 to 0 );
  signal seconds_counter_minute_passed : STD_LOGIC_VECTOR ( 0 to 0 );
  signal seconds_counter_seconds : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal seconds_counter_seconds1 : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal select_bu_1 : STD_LOGIC;
  signal set_button_1 : STD_LOGIC;
  signal set_or_count_button : STD_LOGIC;
  signal util_vector_logic_0_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal util_vector_logic_1_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal util_vector_logic_2_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal util_vector_logic_3_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal util_vector_logic_4_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal util_vector_logic_5_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal util_vector_logic_6_Res : STD_LOGIC_VECTOR ( 0 to 0 );
  signal xlconstant_0_dout : STD_LOGIC_VECTOR ( 0 to 0 );
begin
  CLK_1 <= CLK;
  hours(4 downto 0) <= hours_counter_hours(4 downto 0);
  minutes(5 downto 0) <= seconds_counter_seconds1(5 downto 0);
  seconds(5 downto 0) <= seconds_counter_seconds(5 downto 0);
  select_bu_1 <= select_button;
  set_button_1 <= set_button;
  set_or_count_button <= enable_button;
D_FF_0: component design_1_D_FF_0_12
     port map (
      CLK => set_or_count_button,
      D => D_FF_0_Qbar,
      Q => D_FF_0_Q,
      Qbar => D_FF_0_Qbar,
      R => xlconstant_0_dout(0)
    );
D_FF_1: component design_1_D_FF_0_13
     port map (
      CLK => select_bu_1,
      D => D_FF_1_Qbar,
      Q => D_FF_1_Q,
      Qbar => D_FF_1_Qbar,
      R => xlconstant_0_dout(0)
    );
hours_counter: entity work.hours_counter_imp_JGOEVX
     port map (
      CLK => util_vector_logic_3_Res(0),
      hours(4 downto 0) => hours_counter_hours(4 downto 0)
    );
minutes_counter: entity work.minutes_counter_imp_CMV4NT
     port map (
      CLK => util_vector_logic_4_Res(0),
      hour_passed(0) => minutes_counter_hour_passed(0),
      minutes(5 downto 0) => seconds_counter_seconds1(5 downto 0)
    );
seconds_counter: entity work.seconds_counter_imp_1FQ6MQM
     port map (
      CLK => util_vector_logic_0_Res(0),
      minute_passed(0) => seconds_counter_minute_passed(0),
      second(5 downto 0) => seconds_counter_seconds(5 downto 0)
    );
util_vector_logic_0: component design_1_util_vector_logic_0_5
     port map (
      Op1(0) => CLK_1,
      Op2(0) => D_FF_0_Qbar,
      Res(0) => util_vector_logic_0_Res(0)
    );
util_vector_logic_1: component design_1_util_vector_logic_0_6
     port map (
      Op1(0) => D_FF_0_Q,
      Op2(0) => D_FF_1_Q,
      Res(0) => util_vector_logic_1_Res(0)
    );
util_vector_logic_2: component design_1_util_vector_logic_1_1
     port map (
      Op1(0) => D_FF_0_Q,
      Op2(0) => D_FF_1_Qbar,
      Res(0) => util_vector_logic_2_Res(0)
    );
util_vector_logic_3: component design_1_util_vector_logic_1_2
     port map (
      Op1(0) => util_vector_logic_6_Res(0),
      Op2(0) => minutes_counter_hour_passed(0),
      Res(0) => util_vector_logic_3_Res(0)
    );
util_vector_logic_4: component design_1_util_vector_logic_3_0
     port map (
      Op1(0) => util_vector_logic_5_Res(0),
      Op2(0) => seconds_counter_minute_passed(0),
      Res(0) => util_vector_logic_4_Res(0)
    );
util_vector_logic_5: component design_1_util_vector_logic_2_1
     port map (
      Op1(0) => set_button_1,
      Op2(0) => util_vector_logic_2_Res(0),
      Res(0) => util_vector_logic_5_Res(0)
    );
util_vector_logic_6: component design_1_util_vector_logic_2_2
     port map (
      Op1(0) => util_vector_logic_1_Res(0),
      Op2(0) => set_button_1,
      Res(0) => util_vector_logic_6_Res(0)
    );
xlconstant_0: component design_1_xlconstant_0_0
     port map (
      dout(0) => xlconstant_0_dout(0)
    );
end STRUCTURE;
