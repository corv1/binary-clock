vlib work
vlib riviera

vlib riviera/xil_defaultlib
vlib riviera/xlconcat_v2_1_3
vlib riviera/util_vector_logic_v2_0_1
vlib riviera/xlconstant_v1_1_6

vmap xil_defaultlib riviera/xil_defaultlib
vmap xlconcat_v2_1_3 riviera/xlconcat_v2_1_3
vmap util_vector_logic_v2_0_1 riviera/util_vector_logic_v2_0_1
vmap xlconstant_v1_1_6 riviera/xlconstant_v1_1_6

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_D_FF_0_0/sim/design_1_D_FF_0_0.vhd" \
"../../../bd/design_1/ip/design_1_D_FF_0_1/sim/design_1_D_FF_0_1.vhd" \
"../../../bd/design_1/ip/design_1_D_FF_0_2/sim/design_1_D_FF_0_2.vhd" \
"../../../bd/design_1/ip/design_1_D_FF_0_3/sim/design_1_D_FF_0_3.vhd" \
"../../../bd/design_1/ip/design_1_D_FF_0_4/sim/design_1_D_FF_0_4.vhd" \
"../../../bd/design_1/ip/design_1_D_FF_0_5/sim/design_1_D_FF_0_5.vhd" \

vlog -work xlconcat_v2_1_3  -v2k5 \
"../../../../binary_clock.srcs/sources_1/bd/design_1/ipshared/442e/hdl/xlconcat_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 \
"../../../bd/design_1/ip/design_1_xlconcat_0_0/sim/design_1_xlconcat_0_0.v" \

vlog -work util_vector_logic_v2_0_1  -v2k5 \
"../../../../binary_clock.srcs/sources_1/bd/design_1/ipshared/2137/hdl/util_vector_logic_v2_0_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 \
"../../../bd/design_1/ip/design_1_util_vector_logic_0_0/sim/design_1_util_vector_logic_0_0.v" \
"../../../bd/design_1/ip/design_1_util_vector_logic_0_1/sim/design_1_util_vector_logic_0_1.v" \
"../../../bd/design_1/ip/design_1_util_vector_logic_0_2/sim/design_1_util_vector_logic_0_2.v" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/sim/design_1.vhd" \

vlog -work xil_defaultlib  -v2k5 \
"../../../bd/design_1/ip/design_1_xlconcat_0_1/sim/design_1_xlconcat_0_1.v" \
"../../../bd/design_1/ip/design_1_util_vector_logic_0_3/sim/design_1_util_vector_logic_0_3.v" \
"../../../bd/design_1/ip/design_1_util_vector_logic_1_0/sim/design_1_util_vector_logic_1_0.v" \
"../../../bd/design_1/ip/design_1_util_vector_logic_2_0/sim/design_1_util_vector_logic_2_0.v" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_D_FF_0_8/sim/design_1_D_FF_0_8.vhd" \
"../../../bd/design_1/ip/design_1_D_FF_1_0/sim/design_1_D_FF_1_0.vhd" \
"../../../bd/design_1/ip/design_1_D_FF_2_1/sim/design_1_D_FF_2_1.vhd" \
"../../../bd/design_1/ip/design_1_D_FF_3_0/sim/design_1_D_FF_3_0.vhd" \
"../../../bd/design_1/ip/design_1_D_FF_4_0/sim/design_1_D_FF_4_0.vhd" \
"../../../bd/design_1/ip/design_1_D_FF_5_1/sim/design_1_D_FF_5_1.vhd" \
"../../../bd/design_1/ip/design_1_D_FF_0_9/sim/design_1_D_FF_0_9.vhd" \
"../../../bd/design_1/ip/design_1_D_FF_0_10/sim/design_1_D_FF_0_10.vhd" \
"../../../bd/design_1/ip/design_1_D_FF_0_11/sim/design_1_D_FF_0_11.vhd" \
"../../../bd/design_1/ip/design_1_D_FF_2_2/sim/design_1_D_FF_2_2.vhd" \
"../../../bd/design_1/ip/design_1_D_FF_2_3/sim/design_1_D_FF_2_3.vhd" \

vlog -work xil_defaultlib  -v2k5 \
"../../../bd/design_1/ip/design_1_util_vector_logic_0_4/sim/design_1_util_vector_logic_0_4.v" \
"../../../bd/design_1/ip/design_1_xlconcat_0_3/sim/design_1_xlconcat_0_3.v" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_D_FF_0_12/sim/design_1_D_FF_0_12.vhd" \

vlog -work xil_defaultlib  -v2k5 \
"../../../bd/design_1/ip/design_1_util_vector_logic_0_5/sim/design_1_util_vector_logic_0_5.v" \
"../../../bd/design_1/ip/design_1_util_vector_logic_0_6/sim/design_1_util_vector_logic_0_6.v" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_D_FF_0_13/sim/design_1_D_FF_0_13.vhd" \

vlog -work xil_defaultlib  -v2k5 \
"../../../bd/design_1/ip/design_1_util_vector_logic_1_1/sim/design_1_util_vector_logic_1_1.v" \
"../../../bd/design_1/ip/design_1_util_vector_logic_1_2/sim/design_1_util_vector_logic_1_2.v" \
"../../../bd/design_1/ip/design_1_util_vector_logic_3_0/sim/design_1_util_vector_logic_3_0.v" \

vlog -work xlconstant_v1_1_6  -v2k5 \
"../../../../binary_clock.srcs/sources_1/bd/design_1/ipshared/34f7/hdl/xlconstant_v1_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 \
"../../../bd/design_1/ip/design_1_xlconstant_0_0/sim/design_1_xlconstant_0_0.v" \
"../../../bd/design_1/ip/design_1_util_vector_logic_2_1/sim/design_1_util_vector_logic_2_1.v" \
"../../../bd/design_1/ip/design_1_util_vector_logic_2_2/sim/design_1_util_vector_logic_2_2.v" \

vlog -work xil_defaultlib \
"glbl.v"

